### STAGE 1: Construir ###

# Esta seccion le avisa a docker que solo utilizara esta parte para construir la imagen
FROM node:10-alpine as builder

COPY package.json package-lock.json ./

## Separar mediante "LAYERS" hara que angular no instale paquetes cada vez que construyamos la imagen

RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .

## Compila el angular y lo guarda en la carpeta DIST

RUN npm run ng build -- --prod --output-path=dist


### STAGE 2: Configuracion de Inicio ###

FROM nginx:1.14.1-alpine

## Copia nuestro archivo de Nginx a su carpeta dentro de la imagen de docker
COPY default.conf /etc/nginx/conf.d/

## Borramos el sitio web por default de Nginx
RUN rm -rf /usr/share/nginx/html/*

## Desde la etapa de construccion copiamos los artefactos a la carpeta de Nginx para que este los sirva
COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]